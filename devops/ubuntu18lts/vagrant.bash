#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

# Force Locale
echo "LC_ALL=en_US.UTF-8" >> /etc/default/locale
locale-gen en_US.UTF-8

